package com.szakrevskaya.figuresfx.controller;

import com.szakrevskaya.figuresfx.figures.Circle;
import com.szakrevskaya.figuresfx.figures.Figure;
import com.szakrevskaya.figuresfx.figures.Rectangle;
import com.szakrevskaya.figuresfx.figures.Triangle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.awt.*;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

/**
 * Класс, отвечающий за отрисовку фигур на canvas
 * Будет отлавливать событие клика по кнопку мыши и воспроизводить какие-то действия -
 * отрисовку случайной фигуры - после наступления этого события
 * <p>
 * Перед каждой перерисовкой фигуры мы будем чистить canvas для последующего удобства работы
 * с прогой в будущем и для добавления анимации. А для того, чтобы ее чистить, а все объекты, отрисованные
 * ранее сохранять, чтобы ничего не пропало, заведем массив, куда будем их складывать
 */

public class MainScreenController implements Initializable {
    /**
     * Массив для хранения уже отрисованных объектов
     */
    private Figure[] figures;

    /**
     * Объект класса Random, который будет генерить нам случайные числа заданных диапазонов
     */
    private Random random;

    @FXML
    private Canvas canvas;

    /**
     * @param location
     * @param resources В этой методе, чтобы избежать всяких NullPointerException мы проинициализируем массив,
     *                  иначе у него (массива) будет значение null
     *
     *                  Этот метод вызывается еще до того, как пользователь увидел view и что-либо успел на нем сделать.
     *                  Соответственно, и массив будет инициализироваться еще до того, как пользователь что-то успел нарисовать
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        figures = new Figure[1];
        random = new Random(System.currentTimeMillis());
    }

    /**
     * @param mouseEvent Данный метод будет выполнять какие-то действия после того, как пользователь кликнет на кнопку мыши.
     *                   В нашем случае - будет рисовать фигуру и складывать ее в массив отрисованных фигур.
     *                   Чтобы фигуру нарисовать, ее сначала нужно создать.
     *                   Логику создания и добавления фигуры лучше вынести в отдельные методы:
     *                   createFigure() будет возвращать созданный объект Figure
     *                   addFigure(Figure figure) будет этот созданный объект в массив добавлять.
     */
    @FXML
    private void onMouseClicked(MouseEvent mouseEvent) {
        addFigure(createFigure(mouseEvent.getX(), mouseEvent.getY()));
        repaint();
    }

    /**
     * Метод для отрисовки фигуры
     */
    private void repaint(){
        canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        for(Figure figure : figures){
            if(figure != null){
                figure.draw(canvas.getGraphicsContext2D());
            }
        }
    }

    /**
     * Метод, создающий фигуру.
     * В него передаются координаты фигуры.
     */
    private Figure createFigure(double x, double y) {
        Figure figure = null;
        /**
         * Случайный выбор типа фигуры, который желаем нарисовать (0, 1, 2)
         */
        switch (random.nextInt(3)) {
            case Figure.FIGURE_TYPE_CIRCLE:
                figure = new Circle(x, y, random.nextInt(4), Color.RED, random.nextInt(50));
                break;
            case Figure.FIGURE_TYPE_RECTANGLE:
                figure = new Rectangle(x, y, random.nextInt(4), Color.GREEN, random.nextInt(100), random.nextInt(100));
                break;
            case Figure.FIGURE_TYPE_TRIANGLE:
                figure = new Triangle(x, y, random.nextInt(4), Color.YELLOW, random.nextInt(100));
                break;
            default:
                System.out.println("Unknown figure type!");
        }
        return figure;
    }


    /**
     * @param figure Метод для добавления фигуры в массив
     */
    private void addFigure(Figure figure) {
        /**
         * Проверяем, если последний элемент массива фигур равен null,
         * то кладем в него фигуру и делаем выход из метода
         */
        if (figures[figures.length - 1] == null) {
            figures[figures.length - 1] = figure;
            return;
        }

        /**
         * Если последний элемент массива NULL не равен, создаем новый массив, размерностью на 1 элемент
         * больше, чем оригинальный массив figures, копируем в него все элементы из figures и кладем в него новую фигуру
         */
        Figure[] tmp = new Figure[figures.length + 1];
        int index = 0;
        for (; index < figures.length; index++) {
            tmp[index] = figures[index];
        }
        tmp[index] = figure;

        /**
         * Теперь, чтобы получить новый массив с новой фигурой просто перебиваем ссылку у figures.
         * Делаем так, чтобы это поле ссылалось на tmp.
         * Предыдущий массив просто съест сборщик мусора.
         */
        figures = tmp;

    }
}
